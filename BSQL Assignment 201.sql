USE master
go

DROP DATABASE IF EXISTS EmployeeManagementFSOFT
go

CREATE DATABASE EmployeeManagementFSOFT
GO

USE EmployeeManagementFSOFT
GO

CREATE TABLE Employee
(
	EmpNo INT PRIMARY KEY IDENTITY (1,1),
	EmpName NVARCHAR(50),
	BirthDate DATETIME,
	DeptNo INT,
	MgrNo INT NOT NULL,
	StartDate DATETIME,
	Salary MONEY,
	Level TINYINT CHECK (Level >= 1 AND Level <= 7),
	Status TINYINT CHECK(Status = 0 OR Status =1 OR Status =2),
	Note NVARCHAR(100)
)

--Q2--
----1
ALTER TABLE Employee ADD Email VARCHAR(50)
----2
ALTER TABLE Employee ADD CONSTRAINT DF_MgrNo DEFAULT 0 FOR MgrNo
ALTER TABLE Employee ADD CONSTRAINT DF_Status DEFAULT 0 FOR Status
--Q3--
----1
ALTER TABLE Employee ADD FOREIGN KEY (DeptNo) REFERENCES Department(DeptNo)
ALTER TABLE Emp_Skill DROP COLUMN Description 
CREATE TABLE Skill
(
	SkillNo INT PRIMARY KEY IDENTITY (1,1),
	SkillName NVARCHAR (50),
	Note NVARCHAR(100)
)

CREATE TABLE Emp_Skill
(
	SkillNo INT FOREIGN KEY REFERENCES Skill(SkillNo) NOT NULL ,
	EmpNo INT FOREIGN KEY REFERENCES Employee(EmpNo) NOT NULL ,
	SkillLevel TINYINT CHECK(SkillLevel > =1 AND SkillLevel <=3),
	RegDate DATETIME,
	Description NVARCHAR(100)
)

ALTER TABLE Emp_Skill ADD PRIMARY KEY (SkillNo, EmpNo)

CREATE TABLE Department 
(
	DeptNo INT PRIMARY KEY IDENTITY (1,1),
	DeptName NVARCHAR (50),
	Note NVARCHAR(100)
)
-----------------INSERT--------------------
----EMPLOYEE
insert into Employee (EmpName, BirthDate, DeptNo, MgrNo, StartDate, Salary, Level, Status, Note) values ('Den', '04/18/2005', 2, 5, '12/16/2022', '1078.59', 6, 1, 'Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.');
insert into Employee (EmpName, BirthDate, DeptNo, MgrNo, StartDate, Salary, Level, Status, Note) values ('Seymour', '10/17/2009', 3, 5, '12/6/2022', '1713.00', 7, 0, 'In quis justo.');
insert into Employee (EmpName, BirthDate, DeptNo, MgrNo, StartDate, Salary, Level, Status, Note) values ('Catherine', '12/04/2015', 5, 3, '7/18/2021', '1213.05', 4, 0, 'Aliquam sit amet diam in magna bibendum imperdiet.');
insert into Employee (EmpName, BirthDate, DeptNo, MgrNo, StartDate, Salary, Level, Status, Note) values ('Vivien', '01/27/2016', 1, 5, '5/20/2023', '1421.28', 1, 2, 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo.');
insert into Employee (EmpName, BirthDate, DeptNo, MgrNo, StartDate, Salary, Level, Status, Note) values ('Brett', '05/08/2020', 1, 4, '10/15/2021', '1361.67', 1, 1, 'Integer ac leo.');
----SKILL
insert into Skill (SkillName, Note) values ('LPR', 'Fusce posuere felis sed lacus.');
insert into Skill (SkillName, Note) values ('Lyrics', 'Sed accumsan felis.');
insert into Skill (SkillName, Note) values ('SR&amp;ED', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla.');
insert into Skill (SkillName, Note) values ('Legal Documents', 'Aenean auctor gravida sem.');
insert into Skill (SkillName, Note) values ('Resume Writing', 'Nulla facilisi.');
----Emp_Skill
insert into Emp_Skill (SkillNo, EmpNo, SkillLevel, RegDate) values (3, 1, 3, '1/1/2023');
insert into Emp_Skill (SkillNo, EmpNo, SkillLevel, RegDate) values (1, 2, 3, '1/27/2023');
insert into Emp_Skill (SkillNo, EmpNo, SkillLevel, RegDate) values (2, 4, 1, '12/11/2020');
insert into Emp_Skill (SkillNo, EmpNo, SkillLevel, RegDate) values (1, 4, 1, '1/2/2021');
insert into Emp_Skill (SkillNo, EmpNo, SkillLevel, RegDate) values (2, 5, 2, '6/7/2023');
----Department
insert into Department (DeptName, Note) values ('Research and Development', 'Praesent lectus.');
insert into Department (DeptName, Note) values ('Services', 'Aliquam erat volutpat.');
insert into Department (DeptName, Note) values ('Accounting', 'Nullam varius.');
insert into Department (DeptName, Note) values ('Training', 'Quisque ut erat.');
insert into Department (DeptName, Note) values ('Product Management', 'Donec ut mauris eget massa tempor convallis.');

SELECT * FROM Employee
SELECT * FROM Emp_Skill
SELECT * FROM Skill
SELECT * FROM Department
-----CREATE VIEW
CREATE VIEW EMPLOYEE_TRACKING AS
SELECT e.EmpNo, e.EmpName, e.Level
FROM Employee e
WHERE e.Level IN(3,4,5)

SELECT * FROM EMPLOYEE_TRACKING
